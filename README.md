# ระบบกรอกแบบฟอร์มคำร้องขอสำเร็จการศึกษา

## รายชื่อสมาชิกในกลุ่ม
1. นาย ธิทเดช สระทองอุ่น (รหัส: 644259010, กลุ่ม: 64/46)
2. นาย ทศพล นิลเพชร (รหัส: 644259008, กลุ่ม: 64/46)
3. นาย ณภัทร สายทองสุข (รหัส: 644259030, กลุ่ม: 64/46)
4. นาย กิตติพงษ์ เดชจิต (รหัส: 644259049, กลุ่ม: 64/46)

## Directory Structure

```plaintext
Restaurant
│
├── image
│   ├── babyriki-pandy (1).png
│   └── public
│       ├── babyriki-pandy.svg
│       ├── eaajbdbf5af78fgbbh6kd.svg
│       ├── qweqweq.svg
│       └── vite.svg
│
├── src
│   ├── assets
│   │   └── react.svg
│   │
│   ├── components
│   │   ├── Card
│   │   │   └── Card.jsx
│   │   ├── Carousel
│   │   │   ├── Carousel.css
│   │   │   └── Carousel.jsx
│   │   ├── Footer
│   │   │   └── Footer.jsx
│   │   ├── Loading
│   │   │   └── Loading.jsx
│   │   ├── Navbar
│   │   │   ├── Navbar.jsx
│   │   │   ├── Navbar.category.jsx
│   │   │   └── Navbar.css
│   │   └── Layout.jsx
│   │
│   ├── context
│   │   └── AuthContext.jsx
│   │
│   ├── hook
│   │   └── SearchContext.jsx
│   │
│   ├── loading
│   │   └── restaurant.json
│   │
│   ├── pages
│   │   ├── Add.jsx
│   │   ├── AdminRoute.jsx
│   │   ├── Logout.jsx
│   │   ├── NotAllow.jsx
│   │   ├── Profile.jsx
│   │   ├── ProtectedRoute.jsx
│   │   ├── Restaurant.jsx
│   │   ├── Search.jsx
│   │   ├── Signin.jsx
│   │   ├── Signup.jsx
│   │   └── Update.jsx
│   │
│   ├── services
│   │   └── App.jsx
│   │
│   ├── index.css
│   ├── main.jsx
│   └── .env
│
├── .eslintrc.cjs
├── .gitignore
├── index.html
├── package-lock.json
├── package.json
├── postcss.config.js
└── tailwind.config.js
└── vite.config.js

```
